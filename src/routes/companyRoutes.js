const BaseRoute = require('./base/baseRoute');
const PesquisaHelper = require('../helpers/pesquisaHelper');

const Joi = require('joi');
const Boom = require('boom');

const failAction = (request, headers, erro) => {
    throw erro;
}
const headers = Joi.object({
    authorization: Joi.string().required()
}).unknown();

class CompanyRoutes extends BaseRoute {
    constructor(db) {
        super();
        this._db = db;
    }

    list() {
        return {
            path: '/company',
            method: 'GET',
            config: {
                auth: false,
                cors: true,
                tags: ['api'],
                description: 'Lista todas as empresas',
                notes: 'É possível paginar e e filtrar por nome e tipo',
                validate: {
                    failAction,
                    headers,
                    query: {
                        skip: Joi.number().integer().default(0),
                        limit: Joi.number().integer().default(10),
                        name: Joi.string().min(2).max(100),
                        type: Joi.string().min(2).max(100),
                    },
                },

                handler: (request, headers) => {
                    try {
                        const searchParams = request.query;
                        const query = PesquisaHelper.parametros(searchParams);

                        return this._db.read(query, searchParams.skip, searchParams.limit);
                    } catch (error) {
                        console.error('Erro ao listar', error);
                        return Boom.internal();

                    }
                }
            }
        }
    }

    listOne() {
        return {
            path: '/company/{id}',
            method: 'GET',
            config: {
                auth: false,
                cors: true,
                tags: ['api'],
                description: 'Lista apenas uma empresa',
                notes: 'Busca a empresa somente pelo id especifico',
                validate: {
                    failAction,
                    headers,
                    params: {
                        id: Joi.string().required()
                    }
                }
            },
            handler: async (request) => {
                try {
                    const id = request.params
                    
                    return await this._db.read(id);
                } catch (error) {
                    console.error('Erro ao listar', error);
                    return Boom.internal();
                }
            }
        }
    }

    create() {
        return {
            path: '/company',
            method: 'POST',
            config: {
                tags: ['api'],
                description: 'Cadastra nova empresa',
                notes: 'Cadastro por nome, telefone, endereço e tipo',
                validate: {
                    failAction,
                    headers,
                    payload: {
                        name: Joi.string().required().min(2).max(100),
                        phone: Joi.number().integer(),
                        address: Joi.string().min(3).max(100),
                        type: Joi.string().required().min(2).max(20)
                    }

                }
            },
            handler: async (request) => {
                try {
                    const dados = request.payload;
                    const result = await this._db.create(dados)
                    return {
                        message: 'Cadastrado com sucesso',
                        _id: result._id
                    }
                } catch (error) {
                    console.error('Erro ao cadastrar', error);
                    return Boom.internal();
                }
            }
        }
    }
    update() {
        return {
            path: '/company/{id}',
            method: 'PATCH',
            config: {
                tags: ['api'],
                description: 'Atualiza um registro',
                notes: 'É possível atualizar uma empresa pelo ID',
                validate: {
                    failAction,
                    headers,
                    params: {
                        id: Joi.string().required()
                    },
                    payload: {
                        name: Joi.string().min(2).max(100),
                        phone: Joi.number().integer(),
                        address: Joi.string().min(5).max(100),
                        type: Joi.string().min(2).max(20)
                    }
                }
            },
            handler: async (request) => {
                try {
                    const { id } = request.params;
                    const { payload } = request;
                    const dadosString = JSON.stringify(payload);

                    const dados = JSON.parse(dadosString);

                    const result = await this._db.update(id, dados);
                    if (result.nModified !== 1) return Boom.preconditionFailed('Houve um erro');
                    return {
                        message: 'Atualizado com sucesso'
                    }
                } catch (error) {
                    console.error('Erro ao atualizar', error);
                    return Boom.internal();
                }
            }
        }
    }
    delete() {
        return {
            path: '/company/{id}',
            method: 'DELETE',
            config: {
                tags: ['api'],
                description: 'Apaga um registro',
                notes: 'Deleta um registro pelo ID',
                validate: {
                    failAction,
                    headers,
                    params: {
                        id: Joi.string().required()
                    }
                }
            },
            handler: async (request) => {
                try {
                    const { id } = request.params;
                    const result = await this._db.delete(id);

                    if (result.n !== 1) return Boom.preconditionFailed('Houve um erro');
                    return { message: 'Removido com sucesso' }
                } catch (error) {
                    console.error('Erro ao atualizar', error);
                    return Boom.internal();
                }
            }
        }
    }
}

module.exports = CompanyRoutes;