
class PesquisaHelper {
    static parametros(params) {
        let query = {};

        if (params.name) {
            query = {
                ...query,
                name: { $regex: `.*${params.name}*.` },
            }
        }
        if (params.type) {
            query = {
                ...query,
                type: { $regex: `.*${params.type}*.` },
            }
        }
        return query;
    }
}

module.exports = PesquisaHelper;