const assert = require('assert')
const Mongodb = require('./../database/strategies/mongodb/mongodbStrategy')
const CompanySchema = require('./../database/strategies/mongodb/schemas/companySchema')

const Context = require('./../database/strategies/base/contextStrategy')



let context = {}

const MOCK_COMPANY_CADASTRAR = {
    name: 'Nintendo',
    phone: 31945454545,
    address: 'Japan',
    type: 'videogames',
};
const MOCK_COMPANY_ATUALIZAR = {
    name: `RockStar-${Date.now()}`,
    phone: 31987457845,
    address: 'UnitedStates',
    type: 'badaras',
};
let MOCK_COMPANY_ID = '';
describe('Suite de testes do MongoDb Strategy', function () {
    this.timeout(20000)
    this.beforeAll(async () => {
        const connection = Mongodb.connect();
        context = new Context(new Mongodb(connection, CompanySchema));
        
        const base = await context.create(MOCK_COMPANY_ATUALIZAR);
        MOCK_COMPANY_ID = base._id;
    })
    it('Verifica conexao do mongodb', async () => {
        const result = await context.isConected()

        assert.ok(result === 'Conectado' || result === 'Conectando')
    })
    it('Cadastra company no mongodb', async () => {
        const { name } = await context.create(MOCK_COMPANY_CADASTRAR);

        assert.deepStrictEqual(name, MOCK_COMPANY_CADASTRAR.name)
    })
    it('Lista companies do mongodb', async () => {
        const [{ name }] = await context.read({ name: MOCK_COMPANY_ATUALIZAR.name });

        assert.deepStrictEqual(name, MOCK_COMPANY_ATUALIZAR.name)
    })
    it('Atualiza uma company do mongodb', async () => {
        const result = await context.update(MOCK_COMPANY_ID, {
            name: 'GTAVI'
        });

        assert.deepStrictEqual(result.nModified, 1);
    })
    it('Deleta uma company do mongodb', async () => {
        const result = await context.delete(MOCK_COMPANY_ID);

        assert.deepStrictEqual(result.n, 1);
    })
})