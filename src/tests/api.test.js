const assert = require('assert');
const api = require('./../../api');


const MOCK_COMPANY_CADASTRAR = {
    name: 'Ubisoft',
    phone: 31963636363,
    address: 'USA Street',
    type: 'games'
};
let MOCK_ID = {};
const TOKEN = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MzQsImlhdCI6MTU4OTYzNzczMn0.tf2IZP0YGlperzTA4n08JHlI3RZgrvO2uYLy7nF7FDs';
const headers = {
    authorization: TOKEN
};


describe('Suite de Testes da API', function () {
    this.beforeAll(async () => {
        this.timeout(20000)
        app = await api;
        const result = await app.inject({
            method: 'POST',
            url: '/company',
            headers,
            payload: JSON.stringify(MOCK_COMPANY_CADASTRAR)
        });
        const id = JSON.parse(result.payload);
        MOCK_ID = id._id;
    });

    it('Lista a partir da /company', async () => {
        const result = await app.inject({
            method: 'GET',
            headers,
            url: '/company?skip=0&limit=10'
        });

        const dados = JSON.parse(result.payload);
        const statusCode = result.statusCode;

        assert.deepStrictEqual(statusCode, 200);
        assert.ok(Array.isArray(dados));
    });
    it('Lista de /company com limit correto', async () => {
        const TAMANHO_LIMITE = 3;
        const result = await app.inject({
            method: 'GET',
            headers,
            url: `/company?skip=0&limit=${TAMANHO_LIMITE}`
        });

        const dados = JSON.parse(result.payload);
        const statusCode = result.statusCode;

        assert.deepStrictEqual(statusCode, 200);
        assert.ok(dados.length === TAMANHO_LIMITE)
    });
    it('Lista de /company com paginacao e dá erro de limit', async () => {
        const TAMANHO_LIMITE = 'ASFE';
        const result = await app.inject({
            method: 'GET',
            headers,
            url: `/company?skip=0&limit=${TAMANHO_LIMITE}`
        });
        const statusCode = result.statusCode;

        assert.ok(statusCode !== 200);
    })
    it('Filtra de /company', async () => {
        const TAMANHO_LIMITE = 1000;
        const NAME = MOCK_COMPANY_CADASTRAR.name
        const result = await app.inject({
            method: 'GET',
            headers,
            url: `/company?skip=0&limit=${TAMANHO_LIMITE}&name=${NAME}`
        });
        const [{ name }] = JSON.parse(result.payload)
        const statusCode = result.statusCode;

        assert.deepStrictEqual(statusCode, 200);
        assert.ok(name === NAME)
    });
    it('Listar apenas uma pessoa', async () => {
        const result = await app.inject({
            method: 'GET',
            headers,
            url: `/company/${MOCK_ID}`
        })

        const { _id } = JSON.parse(result.payload);
        const statusCode = result.statusCode;

        assert.ok(statusCode === 200)
        assert.deepStrictEqual(_id, MOCK_ID);
    });
    it('Não lista apenas uma de /company/:id', async () => {
        const FAKE = '5c6b04973c32352ecc6cfd24';
        const result = await app.inject({
            method: 'GET',
            headers,
            url: `/company/${FAKE}`
        });

        const statusCode = result.statusCode;
        
        assert.ok(statusCode === 200)
        assert.ok(!result.payload)
    });
    it('Cadastra em /company', async () => {
        const result = await app.inject({
            method: 'POST',
            headers,
            url: `/company`,
            payload: MOCK_COMPANY_CADASTRAR
        });

        const statusCode = result.statusCode;
        const { message, _id } = JSON.parse(result.payload)
        assert.ok(statusCode === 200);
        assert.notDeepStrictEqual(_id, undefined)
        assert.deepStrictEqual(message, 'Cadastrado com sucesso')
    })
    it('Atualiza item de /company/:id', async () => {
        const expected = {
            type: 'VR'
        }
        const result = await app.inject({
            method: 'PATCH',
            headers,
            url: `/company/${MOCK_ID}`,
            payload: JSON.stringify(expected)

        });
        const statusCode = result.statusCode;
        const dados = JSON.parse(result.payload);

        assert.ok(statusCode === 200);
        assert.deepStrictEqual(dados.message, 'Atualizado com sucesso')
    });
    it('Não atualiza item de /company/:id e dá erro de ID', async () => {
        const expected = {
            type: 'VR'
        }
        const FAKE = '5c6b04973c32352ecc6cfd24';
        const result = await app.inject({
            method: 'PATCH',
            headers,
            url: `/company/${FAKE}`,
            payload: JSON.stringify(expected)
            
        });
        const statusCode = result.statusCode;
        const dados = JSON.parse(result.payload);

        assert.ok(statusCode !== 200);
        assert.deepStrictEqual(dados.message, 'Houve um erro')
    });
    it('Remove de /company/:id', async () => {
        const result = await app.inject({
            method: 'DELETE',
            headers,
            url: `/company/${MOCK_ID}`
        });
        const statusCode = result.statusCode;
        const dados = JSON.parse(result.payload);

        assert.ok(statusCode === 200);
        assert.deepStrictEqual(dados.message, 'Removido com sucesso');

    });
    it('Não remove de /company/:id e dá erro de ID', async () => {
        const FAKE = '5c6b04973c32352ecc6cfd24';
        const result = await app.inject({
            method: 'DELETE',
            headers,
            url: `/company/${FAKE}`
        });
        const statusCode = result.statusCode;
        const dados = JSON.parse(result.payload);

        assert.ok(statusCode !== 200);
        assert.deepStrictEqual(dados.message, 'Houve um erro');

    })
});

