const Mongoose = require('mongoose');

const companySchema = new Mongoose.Schema({
    name: {
        type: String,
        required: true,
    },
    phone: {
        type: Number,
        maxlength: 11,
        minlength: 9,
    },
    address: {
        type: String,
    },
    type: {
        type: String,
    }
});
module.exports = Mongoose.model('company', companySchema);
